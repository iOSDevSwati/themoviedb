//
//  MovieListState.swift
//  TheMovieDB
//
//  Created by Swati Chawla on 12/11/21.
//

import Foundation
import UIKit

class MovieListState: ObservableObject {
    
    @Published var movies: [Movie]? 
    @Published var isLoading: Bool = false
    @Published var error: NSError?

    private let movieService: MovieAPIService
    
    init(movieService: MovieAPIService = MovieStore.shared) {
        self.movieService = movieService
    }
    
    func loadMovies(with endpoint: MovieListEndpoint, inCollectionView collectionView: UICollectionView) {
        self.movies = nil
        self.isLoading = true
        self.movieService.fetchMovies(from: endpoint) { [weak self] (result) in
            guard let self = self else { return }
            self.isLoading = false
            switch result {
            case .success(let response):
                self.movies = response.results
                collectionView.reloadData()
            case .failure(let error):
                self.error = error as NSError
            }
        }
    }
    
}

