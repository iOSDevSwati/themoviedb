//
//  MovieDetailState.swift
//  TheMovieDB
//
//  Created by Swati Chawla on 12/12/21.
//

import SwiftUI

class MovieDetailState: ObservableObject {
    
    private let movieService: MovieAPIService
    @Published var movie: Movie?
    @Published var isLoading = false
    @Published var error: NSError?
    
    init(movieService: MovieAPIService = MovieStore.shared) {
        self.movieService = movieService
    }
    
    func loadMovie(id: Int,inViewController: MovieDetailViewController) {
        self.movie = nil
        self.isLoading = false
        self.movieService.fetchMovie(id: id) {[weak self] (result) in
            guard let self = self else { return }
            
            self.isLoading = false
            switch result {
            case .success(let movie):
                self.movie = movie
                inViewController.setAllMovieDetails(mov: movie)
            case .failure(let error):
                self.error = error as NSError
            }
        }
    }
}

