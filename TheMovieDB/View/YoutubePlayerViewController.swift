//
//  YoutubePlayerViewController.swift
//  TheMovieDB
//
//  Created by Swati Chawla on 12/13/21.
//

import UIKit
import YoutubePlayer_in_WKWebView

class YoutubePlayerViewController : UIViewController {
    
    @IBOutlet weak var youtubeLinkPlayer : WKYTPlayerView!
    var youtubeVideoLink : MovieVideo!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        youtubeLinkPlayer.load(withVideoId: self.youtubeVideoLink.key)
        youtubeLinkPlayer.delegate = self
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        youtubeLinkPlayer.stopVideo()
    }
}

extension YoutubePlayerViewController : WKYTPlayerViewDelegate {
    func playerViewDidBecomeReady(_ playerView: WKYTPlayerView) {
        playerView.playVideo()
    }
}
