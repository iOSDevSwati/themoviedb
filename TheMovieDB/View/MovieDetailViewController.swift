//
//  MovieDetailViewController.swift
//  TheMovieDB
//
//  Created by Swati Chawla on 12/12/21.
//

import UIKit
import SwiftUI

class MovieDetailViewController : UIViewController {
    
    var movie: Movie!
    @ObservedObject private var movieDetailState = MovieDetailState()
    @ObservedObject private var imageLoader = ImageLoader()

    @IBOutlet private weak var movieBackdropImageView : UIImageView!
    @IBOutlet private weak var movieDetailsLabel : UILabel!
    @IBOutlet private weak var movieOverviewTextView : UITextView!
    @IBOutlet private weak var movieRatingLabel : UILabel!
    @IBOutlet private weak var movieCast : UITextView!
    @IBOutlet private weak var movieDirectors : UITextView!
    @IBOutlet private weak var movieProducers : UITextView!
    @IBOutlet private weak var movieScreenWriters : UITextView!
    @IBOutlet private weak var trailerLabel : UILabel!
    @IBOutlet private weak var trailerName1 : UILabel!
    @IBOutlet private weak var trailerName2 : UILabel!
    @IBOutlet private weak var trailerName3 : UILabel!
    @IBOutlet private weak var trailerPlayBtn1 : UIButton!
    @IBOutlet private weak var trailerPlayBtn2 : UIButton!
    @IBOutlet private weak var trailerPlayBtn3 : UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.movieDetailState.loadMovie(id: movie.id, inViewController: self)
        self.imageLoader.loadImage(with: movie.backdropURL, sender: self)
        self.navigationItem.title = movie.title
        if imageLoader.image != nil {
            
            movieBackdropImageView.image = imageLoader.image!
        }
    }
    
    func setAllMovieDetails(mov: Movie) {
        self.movie = mov
        movieDetailsLabel.text = mov.genreText + " - " + mov.yearText + " " + mov.durationText
        movieOverviewTextView.text = mov.overview
        if !mov.ratingText.isEmpty {
            movieRatingLabel.text = mov.ratingText + "   -   " + mov.scoreText
        }
        var castString = ""
        if mov.cast?.isEmpty == false {
            for castMember in mov.cast!.prefix(9) {
                castString.append(castMember.name + " \n")
            }
        }
        movieCast.text = castString
        
        var directorString = ""
        if mov.directors?.isEmpty == false {
            for director in mov.cast!.prefix(2) {
                directorString.append(director.name + " \n")
            }
        }
        movieDirectors.text = directorString
        
        var producerString = ""
        if mov.producers?.isEmpty == false {
            for producer in mov.producers!.prefix(2) {
                producerString.append(producer.name + " \n")
            }
        }
        movieProducers.text = producerString
        
        var screenWriterString = ""
        if mov.screenWriters?.isEmpty == false {
            for screenwriter in mov.screenWriters!.prefix(2) {
                screenWriterString.append(screenwriter.name + " \n")
            }
        }
        movieScreenWriters.text = screenWriterString
        let arrayOfTrailerLabels = [trailerName1,trailerName2,trailerName3]
        let arrayOfTrailerButtons = [trailerPlayBtn1,trailerPlayBtn2,trailerPlayBtn3]
        if mov.youtubeTrailers != nil && mov.youtubeTrailers!.count > 0 {
            for (e1, e2) in zip(arrayOfTrailerLabels.prefix(mov.youtubeTrailers!.count), mov.youtubeTrailers!) {
                e1?.text = e2.name
            }
        } else {
            trailerLabel.text = " "
            for labels in arrayOfTrailerLabels {
                labels?.text = " "
            }
            for buttons in arrayOfTrailerButtons {
                buttons?.imageView?.image = nil
            }
            
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let dest = segue.destination as? YoutubePlayerViewController, let button = sender as? UIButton{
            dest.youtubeVideoLink = self.movie.youtubeTrailers![button.tag-1]
        }
    }
}
