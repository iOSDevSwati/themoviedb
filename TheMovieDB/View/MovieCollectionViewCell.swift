//
//  MovieCollectionViewCell.swift
//  TheMovieDB
//
//  Created by Swati Chawla on 12/10/21.
//

import UIKit

class MovieCollectionViewCell : UICollectionViewCell {
    
    @IBOutlet var imageView : UIImageView!
    @IBOutlet var movieName : UILabel!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        imageView.image = nil
    }
}

