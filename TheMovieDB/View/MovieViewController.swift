//
//  MovieViewController.swift
//  TheMovieDB
//
//  Created by Swati Chawla on 12/10/21.
//

import UIKit
import SwiftUI

class MovieViewController : UIViewController {
    
    @IBOutlet weak var upcomingMovieCollectionView : UICollectionView!
    @IBOutlet weak var nowPlayingMovieCollectionView : UICollectionView!
    @IBOutlet weak var topRatedMovieCollectionView : UICollectionView!
    @IBOutlet weak var popularMovieCollectionView : UICollectionView!

    var movies: [Movie] = []
    @ObservedObject var imageLoader = ImageLoader()
    @ObservedObject private var nowPlayingState = MovieListState()
    @ObservedObject private var upcomingState = MovieListState()
    @ObservedObject private var topRatedState = MovieListState()
    @ObservedObject private var popularState = MovieListState()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.upcomingState.loadMovies(with: .upcoming, inCollectionView: self.upcomingMovieCollectionView)
        self.nowPlayingState.loadMovies(with: .nowPlaying, inCollectionView: self.nowPlayingMovieCollectionView)
        self.topRatedState.loadMovies(with: .topRated, inCollectionView: self.topRatedMovieCollectionView)
        self.popularState.loadMovies(with: .popular, inCollectionView: self.popularMovieCollectionView)
    }
}

extension MovieViewController : UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.upcomingMovieCollectionView {
            return upcomingState.movies?.count ?? 0
        } else if collectionView == self.nowPlayingMovieCollectionView {
            return nowPlayingState.movies?.count ?? 0
        }else if collectionView == self.topRatedMovieCollectionView {
            return topRatedState.movies?.count ?? 0
        }else if collectionView == self.popularMovieCollectionView {
            return popularState.movies?.count ?? 0
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MovieCell", for: indexPath) as? MovieCollectionViewCell else {
            // we failed to get a MovieCell – bail out!
            fatalError("Unable to dequeue MovieCell.")
        }
        
        guard let mov = movieAtIndexPath(indexPath: indexPath, collectionView: collectionView) else {
            return cell
        }
        self.imageLoader.loadImage(with: mov.backdropURL, sender: cell)
        cell.movieName.text = mov.title
        if let imageFromCache = self.imageLoader.imageCache.object(forKey: mov.backdropURL.absoluteString as AnyObject) as? UIImage {
            cell.imageView.image = imageFromCache
        }
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        var myCollectionView : UICollectionView!
        if segue.identifier == "UpcomingMovieDetailSegue" {
            myCollectionView = self.upcomingMovieCollectionView
        } else if segue.identifier == "NowPlayingMovieDetailSegue" {
            myCollectionView = self.nowPlayingMovieCollectionView
        } else if segue.identifier == "TopRatedMovieDetailSegue" {
            myCollectionView = self.topRatedMovieCollectionView
        } else {
            myCollectionView = self.popularMovieCollectionView
        }
        
        if let dest = segue.destination as? MovieDetailViewController, let index = myCollectionView.indexPathsForSelectedItems?.first {
            dest.movie = movieAtIndexPath(indexPath: index, collectionView: myCollectionView)
        }
    }
    
    
    func movieAtIndexPath(indexPath : IndexPath, collectionView : UICollectionView) -> Movie! {
        var mov: Movie!
        if collectionView == self.upcomingMovieCollectionView {
            mov = upcomingState.movies![indexPath.row]
        } else if collectionView == self.nowPlayingMovieCollectionView {
            mov = nowPlayingState.movies![indexPath.row]
        }else if collectionView == self.topRatedMovieCollectionView {
            mov = topRatedState.movies![indexPath.row]
        }else if collectionView == self.popularMovieCollectionView {
            mov = popularState.movies![indexPath.row]
        }
        return mov
    }

}
