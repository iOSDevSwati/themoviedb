//
//  SearchMovieViewController.swift
//  TheMovieDB
//
//  Created by Swati Chawla on 12/13/21.
//

import UIKit
import SwiftUI

class SearchMovieViewController : UIViewController {
    
    @IBOutlet private weak var searchField : UISearchBar!
    @IBOutlet private weak var searchResultsTableView : UITableView!
    
    @ObservedObject var movieSearchState = MovieSearchState()
    var searchActive : Bool = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if movieSearchState.movies != nil {
            if let dest = segue.destination as? MovieDetailViewController, let indexPath = searchResultsTableView.indexPathsForSelectedRows?.first {
                dest.movie = movieSearchState.movies![indexPath.row]
            }
        }
    }
}

extension SearchMovieViewController : UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.movieSearchState.movies != nil {
            return movieSearchState.movies?.count ?? 0
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchCell")! as UITableViewCell
        if self.movieSearchState.movies != nil {
            let mov = movieSearchState.movies![indexPath.row]
            cell.textLabel?.text = mov.title
        }
        return cell
    }
}

extension SearchMovieViewController : UISearchBarDelegate {
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.movieSearchState.search(query: searchText, resultsInTableView: self.searchResultsTableView)
    }
}
